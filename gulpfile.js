"use strict";

var gulp = require("gulp");
var pug	= require("gulp-pug");
var htmlBeautify = require("gulp-html-beautify");
var sass = require('gulp-sass')(require('sass'));
var plumber = require("gulp-plumber");
var cmq	= require("gulp-group-css-media-queries");
var autoPrefixer = require("gulp-autoprefixer");
var cache = require("gulp-cached"); 

// path list
var paths = {
    // path pug render
    pugPathRender: {
        src: "src/pug/*.pug",
        dest: "html/" 
    },

    // path render scss
    scssPathRender: {
        src: "src/scss/*.scss",
        dest: "html/css/"
    },

    // path all pug file
    pugAllPath: "src/pug/**/*.pug",

    // path all scss file
    scssAllPath: "src/scss/**/*.scss"
};

// pug render task
function pugRender(){
    return gulp
    .src(paths.pugPathRender.src)
    .pipe(plumber())
    .pipe(pug())
    .pipe(htmlBeautify({
        indent_char : [" "],
        indent_size : 4,
        wrap_line_length : 0,
        indent_inner_html : true,
        indent_handlebars : true,
        end_with_newline : true,
        preserve_newlines : true,
        unformatted : ["b", "i", "u", "strong", "pre"]
    }))
    .pipe(gulp.dest(paths.pugPathRender.dest));
}

// scss render task
function scssRender(){
    return gulp
    .src(paths.scssPathRender.src)
    .pipe(sass({
        outputStyle: "expanded"
    })
    .on("error", sass.logError))
    .pipe(autoPrefixer([
        "Android 2.3",
        "Android >= 4",
        "Chrome >= 20",
        "Firefox >= 24",
        "Explorer >= 8",
        "iOS >= 6",
        "Opera >= 12",
        "Safari >= 6"
    ]))
    .pipe(cmq())
    .pipe(gulp.dest(paths.scssPathRender.dest));
}

// watch task
function watch(){
    gulp.watch(paths.pugAllPath, pugRender);
    gulp.watch(paths.scssAllPath, scssRender);
}

// run task with parallel
var build = gulp.parallel(pugRender, scssRender, watch);

gulp.task("default", build);
